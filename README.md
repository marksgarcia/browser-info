# Browser Info Footnote
A custom js and css packet that detects the current browser that is being used by the instructor or student, and displays a container for that browser on the user's dashboard upon the initial login to Canvas. 

* **Author:** Mark Garcia

## Usage
To implement this feature, simply do the following:

* **Upload provided css and js files, or add the code to your existing css and js libraries in Canvas.**

The container is triggered and appears when the user's Dashboard is loaded. 
This process is broken into two functions:

	var getBrowserInfo = function(){
		// The code in this function can remain unchanged and is used to detect the browser name and version that is being used by the user
		// The results are returned by the function
	}

	var showBrowserInfo = fucntion() {
		// Uses the information provided by getBrowserInfo() to provide feedback to the user by appending a div to the body of the current Canvas page
		
		// Current usage has been defined for the User Dashboard, but getCurrentPage can be modified to enhance the areas where this is used
		var getCurrentPage = document.getElementsByTagName("title")[0].innerHTML;

		if (getCurrentPage == "User Dashboard") {
	        var browser, getImageSrc, browserHTML;
	        
	        // I have uploaded transparent .png icons for each browser to my OneDrive and shared them publicly
	        var browserIcons = {
	            'safari' : 'https://goo.gl/BzPBtN',
	            'chrome' : 'https://goo.gl/fw1WHe',
	            'firefox' : 'https://goo.gl/44gm4H',
	            'ie' : 'https://goo.gl/CnVSXi',
	            'edge' : 'https://goo.gl/NlfIsd',
	            'opera' : 'https://goo.gl/8UHgvw'
	        };
	        
	        // These lines parse the browser name from the returned value(s) of getBrowserInfo() and then convert them to lowercase
	        browser = getBrowserInfo().split(' ')[0];
	        browser = browser.toLowerCase();
	        
	        // Determines which image should be used based on the browser rerturned by getBrowserInfo() and the corresponding icons from the browserIcons array
	        getImageSrc = browserIcons[browser];
	        
	        // This builds out the minified html that will be appended to the body element. Unless there are additional links that need to be added, this can remain unchanged
	        browserHTML = '<div class="container-footnote"><span class="close-button"></span><img class="browser-img" src="' + getImageSrc + '"/><div class="for-browser-info"><h5>You are currently using ' + getBrowserInfo() + '</h5><a class="button" href="#" target="new">Read More</a></div><div class="additional-info"><a class="link" href="http://www.whatsmybrowser.org" target="new">More info on my browser</a><a class="link" href="https://community.canvaslms.com/docs/DOC-1284" target="new">Is my browser compatible with Canvas?</a><a class="link" href="https://community.canvaslms.com/docs/DOC-2059" target="new">Is my computer compatible with Canvas?</a></div></div>' 
	        
	        // Appends the html to the body element
	        $('body').append(browserHTML)
	    }
	}

## CSS
The css that has been written for the containers has been done under the assumption that font-families, and certain styles will be inherited from the Canvas theme. This means that in the provided index.html file, default serif fonts will be used for all containers. Feel free to modify this as needed to better match your style guides if needed.

## Use cases
* Enhance communication with users regarding their browser
* Ensure that work/assignments are not lost due to incompatabilities between certain browsers and Canvas

## Demo
Simply download the folder and use your preferred browser to open the index.html file found in the demo folder.